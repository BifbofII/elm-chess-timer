module Timer exposing (Action, Timer, countdown, isTimeLeft, run, stopWatch, update, view)

import Time



-- MODEL


type Timer
    = Countdown
        { initial : Int
        , current : Int
        }
    | StopWatch Int



-- ACTIONS


type alias Action =
    Time.Posix



-- CONSTRUCTORS


countdown : Int -> Timer
countdown seconds =
    Countdown { initial = seconds * 10, current = seconds * 10 }


stopWatch : Timer
stopWatch =
    StopWatch 0



-- VIEW


view : Timer -> String
view timer =
    case timer of
        Countdown t ->
            timeToString t.current

        StopWatch t ->
            timeToString t


timeToString : Int -> String
timeToString time =
    let
        tenths =
            padZeros 1 (remainderBy 10 time)

        seconds =
            padZeros 2 (remainderBy 60 (time // 10))

        minutes =
            padZeros 2 (remainderBy 60 (time // 600))

        hours =
            String.fromInt (time // 36000)
    in
    hours ++ ":" ++ minutes ++ ":" ++ seconds ++ "," ++ tenths


padZeros : Int -> Int -> String
padZeros places num =
    let
        numZeros =
            places - String.length (String.fromInt num)
    in
    String.repeat numZeros "0" ++ String.fromInt num



-- UPDATE


update : Timer -> ( Timer, Bool )
update timer =
    case timer of
        Countdown t ->
            if t.current > 1 then
                ( Countdown { t | current = t.current - 1 }, True )

            else
                ( Countdown { t | current = 0 }, False )

        StopWatch t ->
            if t >= 0 then
                ( StopWatch (t + 1), True )

            else
                ( StopWatch 0, False )



-- SUBSCRIPTION


run : (Action -> msg) -> Sub msg
run msg =
    Time.every 100 msg



-- GET INFO


isTimeLeft : Timer -> Bool
isTimeLeft timer =
    case timer of
        Countdown record ->
            record.current > 0

        StopWatch t ->
            t > 0
