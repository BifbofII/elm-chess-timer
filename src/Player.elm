module Player exposing (Player, create, default, initTime, isGameOver, updateTime, view)

import Element exposing (Color, Element, centerX, centerY, column, fill, height, spacing, text, width)
import Element.Background as Background
import Style
import Timer exposing (Timer)



-- TYPE


type Player
    = Player
        { name : String
        , time : Maybe Timer
        , color : Maybe Color
        }



-- DEFAULT


default : Player
default =
    Player
        { name = "Player"
        , time = Nothing
        , color = Nothing
        }



-- MODIFY


initTime : Int -> Player -> Player
initTime duration player =
    case player of
        Player record ->
            Player
                { record | time = Just (Timer.countdown duration) }


updateTime : Player -> ( Player, Bool )
updateTime player =
    case player of
        Player record ->
            case record.time of
                Just timer ->
                    let
                        ( newTimer, running ) =
                            Timer.update timer
                    in
                    ( Player { record | time = Just newTimer }
                    , running
                    )

                Nothing ->
                    ( player, False )



-- GET INFO


isGameOver : Player -> Bool
isGameOver player =
    case player of
        Player record ->
            case record.time of
                Just timer ->
                    not (Timer.isTimeLeft timer)

                Nothing ->
                    False



-- VIEW


view : Player -> Element msg
view player =
    case player of
        Player record ->
            column
                [ width fill
                , height fill
                , spacing Style.spacing
                , Background.color <| maybeColor record.color
                ]
                [ viewName record.name
                , viewTime record.time
                ]


viewName : String -> Element msg
viewName name =
    Element.el
        [ centerX
        , centerY
        ]
    <|
        text name


viewTime : Maybe Timer -> Element msg
viewTime maybeTime =
    case maybeTime of
        Just time ->
            Element.el
                [ centerX
                , centerY
                ]
            <|
                text <|
                    Timer.view time

        Nothing ->
            Element.none


maybeColor : Maybe Color -> Color
maybeColor mColor =
    case mColor of
        Nothing ->
            Element.rgba 0 0 0 0

        Just color ->
            color



-- CREATE


create : String -> Player
create name =
    Player { name = name, time = Nothing, color = Nothing }
