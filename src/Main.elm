module Main exposing (main)

import Browser
import Browser.Events
import Config exposing (Config)
import Element
    exposing
        ( Element
        , alignRight
        , centerX
        , centerY
        , column
        , el
        , fill
        , fillPortion
        , height
        , layout
        , padding
        , rgb255
        , row
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import GameState exposing (GameState)
import Json.Decode as Decode
import Player exposing (Player)
import Style
import Timer



-- MAIN


main : Program Decode.Value Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- TYPES


type Model
    = PreGame Config
    | InGame Config GameState
    | Paused Config GameState
    | PostGame Config Player



-- MESSAGES


type Msg
    = PreGameMsg PreGameMsg
    | InGameMsg InGameMsg
    | PausedMsg PausedMsg
    | PostGameMsg PostGameMsg
    | ResetGame


type PreGameMsg
    = StartGame


type InGameMsg
    = NextPlayer
    | PauseGame
    | TimerTick Timer.Action


type PausedMsg
    = ResumeGame
    | EndGame


type PostGameMsg
    = Restart



-- INIT


init : Decode.Value -> ( Model, Cmd Msg )
init _ =
    ( PreGame Config.default
    , Cmd.none
    )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd msg )
update msg model =
    case ( msg, model ) of
        ( PreGameMsg subMsg, PreGame config ) ->
            case subMsg of
                StartGame ->
                    ( InGame config (GameState.fromConfig config), Cmd.none )

        ( InGameMsg subMsg, InGame _ _ ) ->
            ( updateGame subMsg model, Cmd.none )

        ( PausedMsg subMsg, Paused config state ) ->
            case subMsg of
                ResumeGame ->
                    ( InGame config state, Cmd.none )

                EndGame ->
                    ( PostGame config state.currentPlayer, Cmd.none )

        ( PostGameMsg subMsg, PostGame config _ ) ->
            case subMsg of
                Restart ->
                    ( InGame config (GameState.fromConfig config), Cmd.none )

        ( ResetGame, PostGame config _ ) ->
            ( PreGame config, Cmd.none )

        ( ResetGame, Paused config _ ) ->
            ( PreGame config, Cmd.none )

        ( _, _ ) ->
            ( model, Cmd.none )


updateGame : InGameMsg -> Model -> Model
updateGame msg model =
    case model of
        InGame config state ->
            case msg of
                NextPlayer ->
                    InGame config (GameState.nextTurn state)

                PauseGame ->
                    Paused config state

                TimerTick _ ->
                    let
                        ( newState, gameOver ) =
                            GameState.timeTick state
                    in
                    if gameOver then
                        PostGame config newState.currentPlayer

                    else
                        InGame config newState

        _ ->
            model



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = "Chess Timer"
    , body =
        [ layout
            [ width fill, height fill ]
          <|
            column [ width fill, height fill ]
                [ viewHeader model
                , viewBody model
                , viewFooter
                ]
        ]
    }


viewHeader : Model -> Element Msg
viewHeader model =
    let
        title =
            el
                [ centerX
                , centerY
                , Font.size <| Style.fontSize 5
                ]
            <|
                text "Chess Timer"
    in
    case model of
        PreGame _ ->
            row Style.headerAttributes <| header Element.none title settingsButton

        InGame _ _ ->
            row Style.headerAttributes <| header Element.none title pauseButton

        Paused _ _ ->
            row Style.headerAttributes <|
                header Element.none title <|
                    row
                        [ height fill, width fill, spacing Style.spacing ]
                        [ resetButton, resumeButton, endButton ]

        PostGame _ _ ->
            row Style.headerAttributes <|
                header Element.none title <|
                    row
                        [ height fill, width fill, spacing Style.spacing ]
                        [ restartButton, resetButton ]


header : Element msg -> Element msg -> Element msg -> List (Element msg)
header right center left =
    [ el [ width <| fillPortion 1, height fill ] right
    , el [ width <| fillPortion <| Style.headerMiddlePortion, height fill ] center
    , el [ width <| fillPortion 1, height fill ] left
    ]


settingsButton : Element Msg
settingsButton =
    Input.button
        [ alignRight
        , height fill
        ]
        { label = text "Settings"
        , onPress = Nothing
        }


pauseButton : Element Msg
pauseButton =
    Input.button
        [ alignRight
        , height fill
        ]
        { label = text "Pause"
        , onPress = Just (InGameMsg PauseGame)
        }


resumeButton : Element Msg
resumeButton =
    Input.button
        [ alignRight
        , height fill
        ]
        { label = text "Resume"
        , onPress = Just (PausedMsg ResumeGame)
        }


resetButton : Element Msg
resetButton =
    Input.button
        [ alignRight
        , height fill
        ]
        { label = text "Reset"
        , onPress = Just ResetGame
        }


restartButton : Element Msg
restartButton =
    Input.button
        [ alignRight
        , height fill
        ]
        { label = text "Restart"
        , onPress = Just (PostGameMsg Restart)
        }


endButton : Element Msg
endButton =
    Input.button
        [ alignRight
        , height fill
        ]
        { label = text "End Game"
        , onPress = Just (PausedMsg EndGame)
        }


viewBody : Model -> Element Msg
viewBody model =
    case model of
        PreGame _ ->
            viewPreGame

        InGame _ state ->
            viewInGame state

        Paused _ state ->
            viewInGame state

        PostGame _ winner ->
            viewPostGame winner


viewFooter : Element Msg
viewFooter =
    row
        [ Region.footer
        , width fill
        , Background.color (rgb255 0 255 0)
        , padding Style.padding
        ]
        [ el
            [ centerX
            , Font.size <| Style.fontSize 1
            ]
          <|
            text "Chess Timer by BifbofII"
        ]


viewPreGame : Element Msg
viewPreGame =
    column
        [ centerX
        , centerY
        ]
        [ el
            [ Font.size <| Style.fontSize 6
            , padding Style.padding
            ]
          <|
            text "Welcome to Chess Timer"
        , Input.button
            [ centerX
            , Background.color (rgb255 0 0 255)
            , padding Style.padding
            , Border.rounded 3
            ]
            { label = text "Start Game"
            , onPress = Just <| PreGameMsg <| StartGame
            }
        ]


viewInGame : GameState -> Element Msg
viewInGame state =
    column
        [ height fill
        , width fill
        , Events.onClick (InGameMsg NextPlayer)
        ]
        [ el
            [ height <| fillPortion 2
            , width fill
            ]
          <|
            Player.view state.currentPlayer
        , el
            [ height <| fillPortion 1
            , width fill
            ]
          <|
            Player.view state.nextPlayer
        , el
            [ height <| fillPortion 1
            , width fill
            ]
          <|
            viewOtherPlayers state.otherPlayers
        ]


viewOtherPlayers : List Player -> Element Msg
viewOtherPlayers players =
    row
        [ height fill
        , width fill
        ]
        (List.map Player.view players)


viewPostGame : Player -> Element Msg
viewPostGame winner =
    column
        [ height fill
        , width fill
        ]
        [ el
            [ width fill
            , height <| fillPortion 2
            , Font.center
            , Font.size <| Style.fontSize 7
            ]
          <|
            el [ centerX, centerY ] <|
                text "Congratulations, You won!"
        , el
            [ height <| fillPortion 1
            , width fill
            ]
          <|
            Player.view
                winner
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        InGame _ _ ->
            Sub.batch
                [ Timer.run (\x -> InGameMsg (TimerTick x))
                , Browser.Events.onKeyUp <| onSpace (InGameMsg NextPlayer)
                ]

        _ ->
            Sub.none


onSpace : msg -> Decode.Decoder msg
onSpace msg =
    Decode.field "key" Decode.string
        |> Decode.andThen
            (\key ->
                if key == " " then
                    Decode.succeed msg

                else
                    Decode.fail "Not the space Key"
            )
