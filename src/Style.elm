module Style exposing (fontSize, headerAttributes, headerMiddlePortion, padding, spacing)

import Element
import Element.Background as Background


padding : Int
padding =
    20


spacing : Int
spacing =
    20


fontSize : Int -> Int
fontSize size =
    round <| Element.modular 16 1.25 size


headerAttributes : List (Element.Attribute msg)
headerAttributes =
    [ Element.width Element.fill
    , Element.height <| Element.px 70
    , Background.color (Element.rgb255 255 0 0)
    , Element.padding padding
    ]


headerMiddlePortion : Int
headerMiddlePortion =
    2
