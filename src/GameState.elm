module GameState exposing (GameState, fromConfig, nextTurn, numPlayers, timeTick)

import Config exposing (Config)
import Player exposing (Player)



-- MODEL


type alias GameState =
    { currentPlayer : Player
    , nextPlayer : Player
    , otherPlayers : List Player
    }



-- CREATE


fromConfig : Config -> GameState
fromConfig config =
    let
        ( current, next, other ) =
            Config.getPlayers config
    in
    { currentPlayer = current
    , nextPlayer = next
    , otherPlayers = other
    }


numPlayers : GameState -> Int
numPlayers state =
    2 + List.length state.otherPlayers



-- MODIFY


timeTick : GameState -> ( GameState, Bool )
timeTick state =
    let
        ( player, running ) =
            Player.updateTime state.currentPlayer
    in
    if running then
        ( { state | currentPlayer = player }, False )

    else
        let
            newState =
                nextTurn { state | currentPlayer = player }
        in
        case state.otherPlayers of
            [] ->
                ( newState, True )

            _ :: _ ->
                ( newState, List.all Player.isGameOver (state.nextPlayer :: state.otherPlayers) )


nextTurn : GameState -> GameState
nextTurn state =
    case state.otherPlayers of
        [] ->
            { state
                | currentPlayer = state.nextPlayer
                , nextPlayer = state.currentPlayer
            }

        next :: rest ->
            { state
                | currentPlayer = state.nextPlayer
                , nextPlayer = next
                , otherPlayers = rest ++ [ state.currentPlayer ]
            }
