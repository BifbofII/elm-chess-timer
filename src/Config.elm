module Config exposing (Config, addPlayer, create, default, getPlayers)

import Player exposing (Player)



-- TYPE


type Config
    = Config
        { duration : Int
        , firstPlayer : Player
        , secondPlayer : Player
        , otherPlayers : List Player
        }



-- DEFAULT


default : Config
default =
    create 300 [ "Player 1", "Player 2", "Player 3", "Player 4" ]



-- MODIFY


addPlayer : Player -> Config -> Config
addPlayer player config =
    case config of
        Config record ->
            Config { record | otherPlayers = record.otherPlayers ++ [ player ] }


getPlayers : Config -> ( Player, Player, List Player )
getPlayers config =
    case config of
        Config record ->
            ( Player.initTime record.duration record.firstPlayer
            , Player.initTime record.duration record.secondPlayer
            , List.map (Player.initTime record.duration) record.otherPlayers
            )



-- CREATE


create : Int -> List String -> Config
create duration names =
    case names of
        [] ->
            default

        [ first ] ->
            Config
                { duration = duration
                , firstPlayer = Player.create first
                , secondPlayer = Player.default
                , otherPlayers = []
                }

        first :: second :: rest ->
            Config
                { duration = duration
                , firstPlayer = Player.create first
                , secondPlayer = Player.create second
                , otherPlayers = List.map Player.create rest
                }
